# Contributing with a Twist

This repository hosts [Domain Name System ( DNS )] configuration for
domains that is managed by [Free Software Community of India ( FSCI )].
The configuration is used by [OctoDNS] to sync the DNS configuration
to DigitalOcean which currently hosts all our DNS.

While most of the domains are used for the various services that is
hosted by the community for the world, there are some domains that are
open to the public for associating with their communities.

This is our way of '_contributing_' to the community. There is nothing
much to contribute to this project. But you can make use of our
contribution to your community. You can use sub-domains of the top-level
domains to point to a resource on the internet.

# Available Domains

Currently, we have the following domains that is open for community use :
- fsug.in ( Read : Free Software User Groups )
- ilug.in ( Read : Indian Linux User Groups )

# How to request for a sub-domain

To request for a sub-domain to point to an internet resource, one
needs to open a [GitLab merge request] with the sub-domain of their
choice.

## Crash course on creating merge requests

1. Fork the repository
2. Clone the forked repository to your workstation
3. Make the change in the corresponding domain file under the
	 `domains` directory.
4. `commit` the change
5. `push` the change to your fork
6. create your [new merge request]

Remember to confirm the source branch from where you want to open the
merge request.

# Deployment

The domains are deployed with [GitLab pipelines]. All merge requests
runs a validation job that validates the configuration in the merge
request.

Once the validation step passes, then octoDNS makes a plan for the
change and exposes the plan report as a pipeline artifact which can be
inspected for the change that will be applied by octoDNS should the
merge request be merged.

Once the merge request is approved and merged, then another pipeline
runs on the default branch which blocks at a manual `do-it` job which
has to approved by a community member to be executed. The `do-it` job
will actually deploy the change to DigitalOcean. Considering the time
it takes for DNS propagation, the change may be applied instantly or
may take up to 24 hours or in some rare cases, even longer.

# OctoDNS

## Configuration Format

The configurations are in the popular human-friendly data
serialization format - [YAML].

## Configuration Files

The configuration files are organized within the `domains` directory
with one file per top-level domain.

## Gotchas

Here are some common issues that one may encounter while writing new
sub-domains configurations. OctoDNS validation will fail if these are
ignored.

### Key Ordering

OctoDNS is very strict with key ordering. It likes all the keys to be
ordered in alphabetical order. It will fail validation if the keys are
not properly ordered. Hence, you are encouraged to create your merge
request with the sub-domain ordered alphabetically within the file.

The keys only needs to be the sub-domain part of the domain. For
example, if the sub-domain is `test.example.com`, then the key will
only be `test` and not the fully qualified domain name -
`test.example.com`

### Fully Qualified Domain Names ( FQDN )

Other than the keys of each entry in the configuration file, all other
places where you have to use a domain name, you have to use a
[fully qualified domain name]. And fully qualified domain names always
ends with a dot ( . ). DNS records are parsed from right to left
starting the final dot.

You might be used to omitting the final dot in a domain name when
entering it into a browser. The browser will automatically appended
the final dot for you, but that does not work in DNS zone files; so
you will have to explicitly specify the final dot. If omitted,
validation will fail.


[Domain Name System ( DNS )]: https://en.wikipedia.org/wiki/Domain_Name_System
[Free Software Community of India ( FSCI )]: https://fsci.in
[OctoDNS]: https://github.com/octodns/octodns
[GitLab merge request]: https://docs.gitlab.com/ee/user/project/merge_requests/
[new merge request]: https://gitlab.com/fsci/domains/-/merge_requests/new
[GitLab pipelines]: https://docs.gitlab.com/ee/ci/pipelines/
[YAML]: https://yaml.org
[fully qualified domain name]: https://en.wikipedia.org/wiki/Fully_qualified_domain_name
